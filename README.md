# Settings

This repository contains a template that can be included in a pipeline of a
project to ensure that GitLab settings will be guarded in an entire group to
ensure that it is compliant. Note: the equivalent for GitHub can be found
[here](https://github.com/schubergphilis/mcvs-settings-action).

Once enabled, the following settings will be enforced:
#### Merge requests
* Merge requests are enabled
* Squash will be set to on and cannot be changed when a new Merge Request (MR) has been created
* Merge request method is commit
* Enable "Delete source branch" option by default
* Shows links to create or view a merge request when pushing from the command line
* Pipelines must succeed; Merge requests can't be merged if the latest pipeline did not succeed or is still running
* All threads must be resolved before merging
#### Merge requests approvals
* Approvals required before merging for all branches is set to 1
* Prevent approval by author
* Prevent approvals by users who add commits
* Prevent editing approval rules in merge requests
* Remove all approvals when a commit is added
#### Protected branches
* Only Maintainers are allowed to merge on default branch
* No one is allowed to push and merge on default branch
* Allowed to force push on default branch is disabled
* Code owner approval on default branch is disabled

## Usage

Create a `.gitlab-ci.yml` for example:

For example

```bash
---
variables:
  MCVS_TEMPLATES_SETTINGS_PROJECT_IDS: "55189039 55188901"
  MCVS_TEMPLATES_SETTINGS_FQDN: "gitlab.com"
  SQUASH: "always"
  MERGE_REQUESTS: "true"
  MERGE_METHOD: "merge"
  RM_SOURCE_BRANCH: "true"
  MR_LINK: "true"
  MERGE_PIPELINE_SUCCEEDS: "true"
  MERGE_DISCUSSION_RESOLVED: "true"
  BRANCH_DEFAULT_NAME: "main"

include:
  - remote: https://gitlab.com/schubergphilis/mcvs/templates/settings/-/raw/main/template/.gitlab-ci.yml
```

### Variables:
Enter these variables as strings so in double quotes. 
| Variable | Description |
| --- | --- |
| MCVS_TEMPLATES_SETTINGS_PROJECT_IDS | Project IDs of the relevant projects whose settings are to be guarded |
| MCVS_TEMPLATES_SETTINGS_FQDN | Fully Qualified Domain Name [FQDN](https://en.wikipedia.org/wiki/Fully_qualified_domain_name). |
| SQUASH | "always" = set the default option for squash commits when merging to alays performed. |
| MERGE_REQUESTS | "true" = enables the default merge request option. |
| MR_APPROVALS | "true" = set the number of approvals before merging to 2. |
| MERGE_METHOD | "true" = set the merge method to merge commit. |
| RM_SOURCE_BRANCH | "true" = enables "Delete source branch" option by default. |
| MR_LINK | "true" = shows link to create or view a merge request when pushing form the command line. |
| MERGE_PIPELINE_SUCCEEDS | "true" = merge requests can't be merged if the latest pipeline did not succeed or is still running. | 
| MERGE_DISCUSSION_RESOLVED | "true" = all threads must be resolved before merging. |
| BRANCH_DEFAULT_NAME | "main" = the default branch name (mostly main or master) |

### Include:
Remote refers to the template. In this case it will use the template that resides in this repository.

## Test

In order to test a change, create a branch and adjust the remote to:
`raw/branch-to-be-tested/template` instead of main.
